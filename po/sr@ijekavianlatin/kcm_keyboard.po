# Translation of kcmkeyboard.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2007, 2008, 2010, 2011, 2012, 2014, 2016, 2017.
# Dalibor Djuric <daliborddjuric@gmail.com>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-11 00:40+0000\n"
"PO-Revision-Date: 2017-09-28 17:58+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Časlav Ilić"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "caslav.ilic@gmx.net"

#: bindings.cpp:24
#, fuzzy, kde-format
#| msgid "KDE Keyboard Layout Switcher"
msgid "Keyboard Layout Switcher"
msgstr "KDE‑ov menjač rasporeda tastature"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "Prebaci na sledeći raspored tastera"

#: bindings.cpp:30
#, fuzzy, kde-format
#| msgid "Switch to Next Keyboard Layout"
msgid "Switch to Last-Used Keyboard Layout"
msgstr "Prebaci na sledeći raspored tastera"

#: bindings.cpp:60
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr "Prebaci raspored tastature na %1"

#: keyboardmodel.cpp:35
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "nepoznat"

#: keyboardmodel.cpp:39
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

# >> @title:group
#: tastenbrett/main.cpp:52
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Ponavljanje tastera"

#: tastenbrett/main.cpp:54
#, fuzzy, kde-format
#| msgctxt "tooltip title"
#| msgid "Keyboard Layout"
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "Raspored tastera"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant.\n"
"Previewing layouts that are defined outside your systems xkb directory is "
"not supported and  will also trigger this message. These might still work "
"fine if applied"
msgstr ""

#: ui/Advanced.qml:40
#, fuzzy, kde-format
#| msgid "&Configure keyboard options"
msgctxt "@option:checkbox"
msgid "Configure keyboard options"
msgstr "&Podesi opcije tastature"

#: ui/Hardware.qml:34
#, fuzzy, kde-format
#| msgid "Keyboard &model:"
msgctxt "@title:group"
msgid "Keyboard model:"
msgstr "&Model tastature:"

# rewrite-msgid: /Plasma//
#: ui/Hardware.qml:68
#, fuzzy, kde-format
#| msgid "NumLock on Plasma Startup"
msgctxt "@title:group"
msgid "NumLock on Plasma Startup:"
msgstr "NumLock na pokretanju"

#: ui/Hardware.qml:73
#, fuzzy, kde-format
#| msgid "T&urn on"
msgctxt "@option:radio"
msgid "Turn On"
msgstr "&Uključi"

#: ui/Hardware.qml:77
#, fuzzy, kde-format
#| msgid "&Turn off"
msgctxt "@option:radio"
msgid "Turn Off"
msgstr "&Isključi"

#: ui/Hardware.qml:81
#, fuzzy, kde-format
#| msgid "&Leave unchanged"
msgctxt "@option:radio"
msgid "Leave unchanged"
msgstr "&Ne diraj"

#: ui/Hardware.qml:114
#, kde-format
msgctxt "@title:group"
msgid "When key is held:"
msgstr ""

#: ui/Hardware.qml:119
#, kde-format
msgctxt "@option:radio"
msgid "Repeat the key"
msgstr ""

#: ui/Hardware.qml:124
#, kde-format
msgctxt "@option:radio"
msgid "Do nothing"
msgstr ""

#: ui/Hardware.qml:129
#, kde-format
msgctxt "@option:radio"
msgid "Show accented and similar characters"
msgstr ""

#: ui/Hardware.qml:164
#, fuzzy, kde-format
#| msgid "&Delay:"
msgctxt "@label:slider"
msgid "Delay:"
msgstr "&Zastoj:"

#: ui/Hardware.qml:199
#, fuzzy, kde-format
#| msgid "&Rate:"
msgctxt "@label:slider"
msgid "Rate:"
msgstr "&Učestanost:"

#: ui/Hardware.qml:247
#, fuzzy, kde-format
#| msgid "Test area:"
msgctxt "@label:textbox"
msgid "Test area:"
msgstr "Probna zona:"

#: ui/Hardware.qml:248
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to test settings"
msgstr ""

#: ui/LayoutDialog.qml:21
#, fuzzy, kde-format
#| msgid "Add Layout"
msgctxt "@title:window"
msgid "Add Layout"
msgstr "Dodavanje rasporeda"

#: ui/LayoutDialog.qml:106 ui/Layouts.qml:245
#, fuzzy, kde-format
#| msgid "Preview"
msgctxt "@action:button"
msgid "Preview"
msgstr "Pregled"

#: ui/LayoutDialog.qml:124
#, kde-format
msgctxt "@label:textbox"
msgid "Display text:"
msgstr ""

#: ui/LayoutDialog.qml:143
#, fuzzy, kde-format
#| msgid "Shortcut:"
msgctxt "@option:textbox"
msgid "Shortcut:"
msgstr "Prečica:"

#: ui/Layouts.qml:28
#, fuzzy, kde-format
#| msgid "Shortcuts for Switching Layout"
msgctxt "@title:group"
msgid "Shortcuts for Switching Layout"
msgstr "Prečice za prebacivanje rasporeda"

#: ui/Layouts.qml:34
#, fuzzy, kde-format
#| msgid "Main shortcuts:"
msgctxt "@option:textbox"
msgid "Main shortcuts:"
msgstr "Glavne prečice:"

#: ui/Layouts.qml:61
#, fuzzy, kde-format
#| msgid "3rd level shortcuts:"
msgctxt "@option:textbox"
msgid "3rd level shortcuts:"
msgstr "Prečice za treći nivo:"

#: ui/Layouts.qml:88
#, fuzzy, kde-format
#| msgid "Alternative shortcut:"
msgctxt "@option:textbox"
msgid "Alternative shortcut:"
msgstr "Alternativna prečica:"

#: ui/Layouts.qml:109
#, fuzzy, kde-format
#| msgid "Alternative shortcut:"
msgctxt "@option:textbox"
msgid "Last used shortcuts:"
msgstr "Alternativna prečica:"

#: ui/Layouts.qml:129
#, kde-format
msgctxt "@option:checkbox"
msgid "Show a popup on layout changes"
msgstr ""

#: ui/Layouts.qml:145
#, fuzzy, kde-format
#| msgid "Switching Policy"
msgctxt "@title:group"
msgid "Switching Policy"
msgstr "Smjernica mijenjanja"

#: ui/Layouts.qml:152
#, fuzzy, kde-format
#| msgid "&Global"
msgctxt "@option:radio"
msgid "Global"
msgstr "&Globalno"

#: ui/Layouts.qml:156
#, fuzzy, kde-format
#| msgid "&Desktop"
msgctxt "@option:radio"
msgid "Desktop"
msgstr "Po po&vrši"

#: ui/Layouts.qml:160
#, fuzzy, kde-format
#| msgid "&Application"
msgctxt "@option:radio"
msgid "Application"
msgstr "Po &programu"

#: ui/Layouts.qml:164
#, fuzzy, kde-format
#| msgid "&Window"
msgctxt "@option:radio"
msgid "Window"
msgstr "Po pro&zoru"

#: ui/Layouts.qml:185
#, fuzzy, kde-format
#| msgid "Configure layouts"
msgctxt "@option:checkbox"
msgid "Configure Layouts"
msgstr "Podesi rasporede"

#: ui/Layouts.qml:200
#, fuzzy, kde-format
#| msgid "Add"
msgctxt "@action:button"
msgid "Add"
msgstr "Dodaj"

#: ui/Layouts.qml:206
#, fuzzy, kde-format
#| msgid "Remove"
msgctxt "@action:button"
msgid "Remove"
msgstr "Ukloni"

#: ui/Layouts.qml:213
#, fuzzy, kde-format
#| msgid "Move Up"
msgctxt "@action:button"
msgid "Move Up"
msgstr "Pomeri nagore"

#: ui/Layouts.qml:229
#, fuzzy, kde-format
#| msgid "Move Down"
msgctxt "@action:button"
msgid "Move Down"
msgstr "Pomeri nadole"

# >> @title:column
#: ui/Layouts.qml:316
#, fuzzy, kde-format
#| msgctxt "layout map name"
#| msgid "Map"
msgctxt "@title:column"
msgid "Map"
msgstr "mapa"

# >> @title:column
#: ui/Layouts.qml:323
#, fuzzy, kde-format
#| msgid "Label"
msgctxt "@title:column"
msgid "Label"
msgstr "etiketa"

# >> @title:column
#: ui/Layouts.qml:332
#, fuzzy, kde-format
#| msgid "Layout"
msgctxt "@title:column"
msgid "Layout"
msgstr "raspored"

# >> @title:column
#: ui/Layouts.qml:337
#, fuzzy, kde-format
#| msgid "Variant"
msgctxt "@title:column"
msgid "Variant"
msgstr "varijanta"

# >> @title:column
#: ui/Layouts.qml:342
#, fuzzy, kde-format
#| msgid "Shortcut"
msgctxt "@title:column"
msgid "Shortcut"
msgstr "prečica"

#: ui/Layouts.qml:388
#, fuzzy, kde-format
#| msgid "Main shortcuts:"
msgctxt "@action:button"
msgid "Reassign shortcut"
msgstr "Glavne prečice:"

#: ui/Layouts.qml:404
#, kde-format
msgctxt "@action:button"
msgid "Cancel assignment"
msgstr ""

#: ui/Layouts.qml:417
#, fuzzy, kde-format
#| msgid "Spare layouts"
msgctxt "@option:checkbox"
msgid "Spare layouts"
msgstr "Rezervni rasporedi"

#: ui/Layouts.qml:453
#, fuzzy, kde-format
#| msgid "Main layout count:"
msgctxt "@label:spinbox"
msgid "Main layout count:"
msgstr "Broj glavnih rasporeda:"

#: ui/main.qml:43
#, fuzzy, kde-format
#| msgid "Hardware"
msgctxt "@title:tab"
msgid "Hardware"
msgstr "Hardver"

#: ui/main.qml:49
#, fuzzy, kde-format
#| msgid "Layouts"
msgctxt "@title:tab"
msgid "Layouts"
msgstr "Rasporedi"

#: ui/main.qml:55
#, kde-format
msgctxt "@title:tab"
msgid "Key Bindings"
msgstr ""

#: xkboptionsmodel.cpp:160
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] "%1 prečica"
msgstr[1] "%1 prečice"
msgstr[2] "%1 prečica"
msgstr[3] "%1 prečica"

#: xkboptionsmodel.cpp:163
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "ništa"

#~ msgctxt "layout - variant"
#~ msgid "%1 - %2"
#~ msgstr "%1 — %2"

#~ msgid "Label:"
#~ msgstr "Etiketa:"

#~ msgid ""
#~ "Here you can choose a keyboard model. This setting is independent of your "
#~ "keyboard layout and refers to the \"hardware\" model, i.e. the way your "
#~ "keyboard is manufactured. Modern keyboards that come with your computer "
#~ "usually have two extra keys and are referred to as \"104-key\" models, "
#~ "which is probably what you want if you do not know what kind of keyboard "
#~ "you have.\n"
#~ msgstr ""
#~ "Ovdje možete izabrati model tastature. Ova postavka ne zavisi od "
#~ "rasporeda tastature, već se tiče „hardverskog“ modela, tj. načina na koji "
#~ "je tastatura proizvedena. Moderne tastature obično imaju dva dodatna "
#~ "tastera i poznate su kao „104‑tasterski“ modeli, što je dobar osnovni "
#~ "izbor ako ne znate tačno kakvu tastaturu imate.\n"

#~ msgid ""
#~ "If you select \"Application\" or \"Window\" switching policy, changing "
#~ "the keyboard layout will only affect the current application or window."
#~ msgstr ""
#~ "Ako izaberete smernicu menjanja po programu ili po prozoru, promjena "
#~ "rasporeda će uticati samo na tekući program ili prozor."

#~ msgid ""
#~ "This is a shortcut for switching layouts which is handled by X.org. It "
#~ "allows modifier-only shortcuts."
#~ msgstr ""
#~ "Ovo je prečica za mijenjanje rasporeda koju obrađuje X. Dopušta čisto "
#~ "modifikatorske prečice."

#~ msgctxt "no shortcut defined"
#~ msgid "None"
#~ msgstr "Nikakva"

#~ msgid ""
#~ "This is a shortcut for switching to a third level of the active layout "
#~ "(if it has one) which is handled by X.org. It allows modifier-only "
#~ "shortcuts."
#~ msgstr ""
#~ "Ovo je prečica za prelazak na treći nivo aktivnog rasporeda (ako postoji) "
#~ "koju obrađuje X. Dopušta čisto modifikatorske prečice."

#~ msgid ""
#~ "This is a shortcut for switching layouts. It does not support modifier-"
#~ "only shortcuts and also may not work in some situations (e.g. if popup is "
#~ "active or from screensaver)."
#~ msgstr ""
#~ "Ovo je prečica za menjanje rasporeda. Ne podržava čisto modifikatorske "
#~ "prečice i može da ne radi u nekim slučajevima (npr. ako je aktivan "
#~ "iskakač ili na čuvaru ekrana)."

#~ msgid "Advanced"
#~ msgstr "Napredno"

# >> @item:inlistbox layout variant
#~ msgctxt "variant"
#~ msgid "Default"
#~ msgstr "podrazumevani"

#~ msgid ""
#~ "Allows to test keyboard repeat and click volume (just don't forget to "
#~ "apply the changes)."
#~ msgstr ""
#~ "Omogućava da se isproba ponavljanje tastature i glasnost klika (ne "
#~ "zaboravite da prethodno primenite izmene)."

#~ msgid ""
#~ "If supported, this option allows you to setup the state of NumLock after "
#~ "Plasma startup.<p>You can configure NumLock to be turned on or off, or "
#~ "configure Plasma not to set NumLock state."
#~ msgstr ""
#~ "<p>Ako je podržana, ovom opcijom određujete stanje tastera NumLock po "
#~ "pokretanju Plasme.</p><p>Možete podesiti da je NumLock uključen ili "
#~ "isključen, ili da Plasma ne mijenja zatečeno stanje.</p>"

#~ msgid "Leave unchan&ged"
#~ msgstr "&Ne diraj"

#~ msgid ""
#~ "If supported, this option allows you to set the delay after which a "
#~ "pressed key will start generating keycodes. The 'Repeat rate' option "
#~ "controls the frequency of these keycodes."
#~ msgstr ""
#~ "<html>Ako je podržana, ovom opcijom zadajete zastoj prije nego što "
#~ "pritisnuti taster počne da šalje znakove. Brzinu izdavanja znakova "
#~ "kontroliše opcija <i>Učestanost:</i>.</html>"

#~ msgid ""
#~ "If supported, this option allows you to set the rate at which keycodes "
#~ "are generated while a key is pressed."
#~ msgstr ""
#~ "Ako je podržana, ovom opcijom zadajete učestanost kojom se znakovi šalju "
#~ "dok je taster pritisnut."

# well-spelled: сек
#~ msgid " repeats/s"
#~ msgstr " znak./sek."

#~ msgid " ms"
#~ msgstr " ms"

#~ msgid "KDE Keyboard Control Module"
#~ msgstr "KDE kontrolni modul tastature"

#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "© 2010, Andrij Risin"

#~ msgid ""
#~ "<h1>Keyboard</h1> This control module can be used to configure keyboard "
#~ "parameters and layouts."
#~ msgstr ""
#~ "<h1>Tastatura</h1><p>Ova kontrolni modul služi za podešavanje parametara "
#~ "i rasporeda tastature.</p>"

#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "KDE‑ov menjač rasporeda tastature"

#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 — %2"

# >> @item:inlistbox
#~ msgid "Any language"
#~ msgstr "bilo koji jezik"

# >> @label:listbox
#~ msgid "Layout:"
#~ msgstr "Raspored:"

# >> @label:listbox
#~ msgid "Variant:"
#~ msgstr "Varijanta:"

#~ msgid "Limit selection by language:"
#~ msgstr "Ograniči izbor po jeziku:"

#~ msgid "Layout Indicator"
#~ msgstr "Pokazatelj rasporeda"

# >> @option:checkbox Layout Indicator
# rewrite-msgid: /layout//
#~ msgid "Show layout indicator"
#~ msgstr "Prikaži pokazatelj"

# >> @option:checkbox Layout Indicator
# rewrite-msgid: /layout//
#~ msgid "Show for single layout"
#~ msgstr "I pri jednom rasporedu"

# >> @option:checkbox Layout Indicator
#~ msgid "Show flag"
#~ msgstr "Sa zastavom"

# >> @option:checkbox Layout Indicator
#~ msgid "Show label"
#~ msgstr "Sa etiketom"

# >> @option:checkbox Layout Indicator
#~ msgid "Show label on flag"
#~ msgstr "Sa etiketom na zastavi"

#~ msgid "..."
#~ msgstr "..."

#~ msgid "Only up to %1 keyboard layout is supported"
#~ msgid_plural "Only up to %1 keyboard layouts are supported"
#~ msgstr[0] "Moguće je dodati najviše %1 raspored"
#~ msgstr[1] "Moguće je dodati najviše %1 rasporeda"
#~ msgstr[2] "Moguće je dodati najviše %1 rasporeda"
#~ msgstr[3] "Moguće je dodati najviše %1 raspored"

# >> @title:group
#~ msgid "Keyboard Repeat"
#~ msgstr "Ponavljanje tastera"

#~ msgid "Turn o&ff"
#~ msgstr "&Isključi"

#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "Raspored tastera"

#~ msgid "Configure Layouts..."
#~ msgstr "Podesi rasporede..."
