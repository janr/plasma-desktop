# Malayalam translation of kcmkeyboard.
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the kcmkeyboard package.
# Manu S Madhav <manusmad@gmail.com>, 2008.
# Anish A <anish.nl@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard trunk\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-11 00:40+0000\n"
"PO-Revision-Date: 2019-12-12 22:30+0000\n"
"Last-Translator: Vivek KJ Pazhedath <vivekkj2004@gmail.com>\n"
"Language-Team: SMC <smc.org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Manu S Madhav, Anish A (അനീഷ് എ)"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "shijualexonline@gmail.com,snalledam@dataone.in,vivekkj2004@gmail.com"

#: bindings.cpp:24
#, fuzzy, kde-format
msgid "Keyboard Layout Switcher"
msgstr "അടുത്ത കീബോര്‍ഡു് വിന്യാസത്തിലേയ്ക്കു് മാറ്റുക"

#: bindings.cpp:26
#, fuzzy, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "അടുത്ത കീബോര്‍ഡു് വിന്യാസത്തിലേയ്ക്കു് മാറ്റുക"

#: bindings.cpp:30
#, fuzzy, kde-format
msgid "Switch to Last-Used Keyboard Layout"
msgstr "അടുത്ത കീബോര്‍ഡു് വിന്യാസത്തിലേയ്ക്കു് മാറ്റുക"

#: bindings.cpp:60
#, fuzzy, kde-format
msgid "Switch keyboard layout to %1"
msgstr "'%1' കീബോര്‍ഡു് വിന്യാസത്തിലേയ്ക്കു് മാറ്റുന്നതില്‍ പിശകു്"

#: keyboardmodel.cpp:35
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "അറിഞ്ഞുകൂടാത്ത"

#: keyboardmodel.cpp:39
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

#: tastenbrett/main.cpp:52
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ്"

#: tastenbrett/main.cpp:54
#, fuzzy, kde-format
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "അടുത്ത കീബോര്‍ഡു് വിന്യാസത്തിലേയ്ക്കു് മാറ്റുക"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant.\n"
"Previewing layouts that are defined outside your systems xkb directory is "
"not supported and  will also trigger this message. These might still work "
"fine if applied"
msgstr ""

#: ui/Advanced.qml:40
#, fuzzy, kde-format
msgctxt "@option:checkbox"
msgid "Configure keyboard options"
msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ് &പ്രാവര്‍ത്തികമാക്കുക"

#: ui/Hardware.qml:34
#, fuzzy, kde-format
msgctxt "@title:group"
msgid "Keyboard model:"
msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ്"

#: ui/Hardware.qml:68
#, fuzzy, kde-format
msgctxt "@title:group"
msgid "NumLock on Plasma Startup:"
msgstr "കെഡിഇ തുടങ്ങുമ്പോള്‍ നംലോക്ക് (NumLock)"

#: ui/Hardware.qml:73
#, fuzzy, kde-format
#| msgid "T&urn on"
msgctxt "@option:radio"
msgid "Turn On"
msgstr "&ഓണാക്കുക"

#: ui/Hardware.qml:77
#, fuzzy, kde-format
msgctxt "@option:radio"
msgid "Turn Off"
msgstr "ഓ&ഫാക്കുക"

#: ui/Hardware.qml:81
#, fuzzy, kde-format
msgctxt "@option:radio"
msgid "Leave unchanged"
msgstr "&മാറ്റാതെ വിടുക"

#: ui/Hardware.qml:114
#, kde-format
msgctxt "@title:group"
msgid "When key is held:"
msgstr ""

#: ui/Hardware.qml:119
#, kde-format
msgctxt "@option:radio"
msgid "Repeat the key"
msgstr ""

#: ui/Hardware.qml:124
#, kde-format
msgctxt "@option:radio"
msgid "Do nothing"
msgstr ""

#: ui/Hardware.qml:129
#, kde-format
msgctxt "@option:radio"
msgid "Show accented and similar characters"
msgstr ""

#: ui/Hardware.qml:164
#, fuzzy, kde-format
#| msgid "&Delay:"
msgctxt "@label:slider"
msgid "Delay:"
msgstr "&വൈകല്‍:"

#: ui/Hardware.qml:199
#, fuzzy, kde-format
#| msgid "&Rate:"
msgctxt "@label:slider"
msgid "Rate:"
msgstr "&വേഗം:"

#: ui/Hardware.qml:247
#, kde-format
msgctxt "@label:textbox"
msgid "Test area:"
msgstr ""

#: ui/Hardware.qml:248
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to test settings"
msgstr ""

#: ui/LayoutDialog.qml:21
#, fuzzy, kde-format
msgctxt "@title:window"
msgid "Add Layout"
msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ്"

#: ui/LayoutDialog.qml:106 ui/Layouts.qml:245
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "@action:button"
msgid "Preview"
msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ്"

#: ui/LayoutDialog.qml:124
#, kde-format
msgctxt "@label:textbox"
msgid "Display text:"
msgstr ""

#: ui/LayoutDialog.qml:143
#, fuzzy, kde-format
msgctxt "@option:textbox"
msgid "Shortcut:"
msgstr "%1 കുറുക്കുവഴി"

#: ui/Layouts.qml:28
#, fuzzy, kde-format
msgctxt "@title:group"
msgid "Shortcuts for Switching Layout"
msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ് &പ്രാവര്‍ത്തികമാക്കുക"

#: ui/Layouts.qml:34
#, fuzzy, kde-format
msgctxt "@option:textbox"
msgid "Main shortcuts:"
msgstr "%1 കുറുക്കുവഴി"

#: ui/Layouts.qml:61
#, kde-format
msgctxt "@option:textbox"
msgid "3rd level shortcuts:"
msgstr ""

#: ui/Layouts.qml:88
#, kde-format
msgctxt "@option:textbox"
msgid "Alternative shortcut:"
msgstr ""

#: ui/Layouts.qml:109
#, kde-format
msgctxt "@option:textbox"
msgid "Last used shortcuts:"
msgstr ""

#: ui/Layouts.qml:129
#, kde-format
msgctxt "@option:checkbox"
msgid "Show a popup on layout changes"
msgstr ""

#: ui/Layouts.qml:145
#, kde-format
msgctxt "@title:group"
msgid "Switching Policy"
msgstr ""

#: ui/Layouts.qml:152
#, kde-format
msgctxt "@option:radio"
msgid "Global"
msgstr ""

#: ui/Layouts.qml:156
#, kde-format
msgctxt "@option:radio"
msgid "Desktop"
msgstr ""

#: ui/Layouts.qml:160
#, kde-format
msgctxt "@option:radio"
msgid "Application"
msgstr ""

#: ui/Layouts.qml:164
#, kde-format
msgctxt "@option:radio"
msgid "Window"
msgstr ""

#: ui/Layouts.qml:185
#, fuzzy, kde-format
msgctxt "@option:checkbox"
msgid "Configure Layouts"
msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ് &പ്രാവര്‍ത്തികമാക്കുക"

#: ui/Layouts.qml:200
#, kde-format
msgctxt "@action:button"
msgid "Add"
msgstr ""

#: ui/Layouts.qml:206
#, kde-format
msgctxt "@action:button"
msgid "Remove"
msgstr ""

#: ui/Layouts.qml:213
#, kde-format
msgctxt "@action:button"
msgid "Move Up"
msgstr ""

#: ui/Layouts.qml:229
#, kde-format
msgctxt "@action:button"
msgid "Move Down"
msgstr ""

#: ui/Layouts.qml:316
#, fuzzy, kde-format
#| msgctxt "layout map name"
#| msgid "Map"
msgctxt "@title:column"
msgid "Map"
msgstr "മാപ്പ്"

#: ui/Layouts.qml:323
#, fuzzy, kde-format
#| msgid "Label"
msgctxt "@title:column"
msgid "Label"
msgstr "പേരു്"

#: ui/Layouts.qml:332
#, fuzzy, kde-format
#| msgid "Layout"
msgctxt "@title:column"
msgid "Layout"
msgstr "വിന്യാസം"

#: ui/Layouts.qml:337
#, fuzzy, kde-format
#| msgid "Variant"
msgctxt "@title:column"
msgid "Variant"
msgstr "മാറ്റമുള്ള"

#: ui/Layouts.qml:342
#, fuzzy, kde-format
msgctxt "@title:column"
msgid "Shortcut"
msgstr "%1 കുറുക്കുവഴി"

#: ui/Layouts.qml:388
#, kde-format
msgctxt "@action:button"
msgid "Reassign shortcut"
msgstr ""

#: ui/Layouts.qml:404
#, kde-format
msgctxt "@action:button"
msgid "Cancel assignment"
msgstr ""

#: ui/Layouts.qml:417
#, fuzzy, kde-format
msgctxt "@option:checkbox"
msgid "Spare layouts"
msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ് &പ്രാവര്‍ത്തികമാക്കുക"

#: ui/Layouts.qml:453
#, kde-format
msgctxt "@label:spinbox"
msgid "Main layout count:"
msgstr ""

#: ui/main.qml:43
#, kde-format
msgctxt "@title:tab"
msgid "Hardware"
msgstr ""

#: ui/main.qml:49
#, fuzzy, kde-format
#| msgid "Layout"
msgctxt "@title:tab"
msgid "Layouts"
msgstr "വിന്യാസം"

#: ui/main.qml:55
#, kde-format
msgctxt "@title:tab"
msgid "Key Bindings"
msgstr ""

#: xkboptionsmodel.cpp:160
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] ""
msgstr[1] ""

#: xkboptionsmodel.cpp:163
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "ഒന്നുമില്ല"

#, fuzzy
#~ msgctxt "layout - variant"
#~ msgid "%1 - %2"
#~ msgstr "%1 | %2"

#~ msgctxt "variant"
#~ msgid "Default"
#~ msgstr "സഹജമായ"

#, fuzzy
#~ msgid ""
#~ "If supported, this option allows you to setup the state of NumLock after "
#~ "Plasma startup.<p>You can configure NumLock to be turned on or off, or "
#~ "configure Plasma not to set NumLock state."
#~ msgstr ""
#~ "പിന്തുണയുണ്ടെങ്കില്‍, കെഡിഇ തുടങ്ങിക്കഴിഞ്ഞുള്ള നംലോക്കിന്റെ സ്ഥിതി സജ്ജീകരിക്കാന്‍ ഈ ഐച്ഛികം "
#~ "നിങ്ങളെ അനുവദിക്കും.<p> നംലോക്ക് ഓണാക്കാനോ, ഓഫാക്കാനോ, കെഡിഇ നംലോക്കിന്റെ സ്ഥിതി "
#~ "സജ്ജീകരിക്കാതിരിക്കാനോ നിങ്ങള്‍ക്ക് ക്രമീകരിക്കാം."

#~ msgid "Leave unchan&ged"
#~ msgstr "&മാറ്റാതെ വിടുക"

#~ msgid ""
#~ "If supported, this option allows you to set the delay after which a "
#~ "pressed key will start generating keycodes. The 'Repeat rate' option "
#~ "controls the frequency of these keycodes."
#~ msgstr ""
#~ "പിന്തുണയുണ്ടെങ്കില്‍, ഒരു കീ അമര്‍ത്തിക്കഴിഞ്ഞ് കീക്കോഡുകള്‍ ഉല്‍പാദിപ്പിക്കാനെടുക്കുന്ന സമയം "
#~ "സജ്ജീകരിക്കാന്‍ ഈ ഐച്ഛികം നിങ്ങളെ അനുവദിക്കും. 'റിപ്പീറ്റ് വേഗം' ഐച്ഛികം ഈ കീക്കോഡുകളുടെ "
#~ "ആവൃത്തി നിയന്ത്രിക്കുന്നു."

#~ msgid ""
#~ "If supported, this option allows you to set the rate at which keycodes "
#~ "are generated while a key is pressed."
#~ msgstr ""
#~ "പിന്തുണയുണ്ടെങ്കില്‍, ഒരു കീ അമര്‍ത്തിക്കൊണ്ടിരിക്കുമ്പോള്‍ കീക്കോഡുകള്‍ ഉല്‍പാദിപ്പിക്കുന്ന വേഗം "
#~ "സജ്ജീകരിക്കാന്‍ ഈ ഐച്ഛികം നിങ്ങളെ അനുവദിക്കും."

#~ msgid " repeats/s"
#~ msgstr " ആവര്‍ത്തനം/സെ"

#~ msgid " ms"
#~ msgstr " മി. സെ"

#~ msgid "KDE Keyboard Control Module"
#~ msgstr "കെഡിഇ കീബോര്‍ഡു് ഘടന മാറ്റലുപാധി"

#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "പകര്‍പ്പവകാശം (C) 2006-2007 ആന്‍ഡ്രി റിസിന്‍"

#~ msgid ""
#~ "<h1>Keyboard</h1> This control module can be used to configure keyboard "
#~ "parameters and layouts."
#~ msgstr ""
#~ "<h1>കീബോര്‍ഡ് </h1> ഈ നിയന്ത്രണ മോഡ്യൂള്‍ കീബോര്‍ഡ് പരാമീറ്ററുകളും വിന്യാസങ്ങളും "
#~ "പരിഷ്ക്കരിക്കാന്‍ ഉപയോഗിക്കാവന്നതാണ്."

#, fuzzy
#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "അടുത്ത കീബോര്‍ഡു് വിന്യാസത്തിലേയ്ക്കു് മാറ്റുക"

#~ msgid "Any language"
#~ msgstr "ഏതു ഭാഷയും"

#, fuzzy
#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 | %2"

#, fuzzy
#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ്"

#, fuzzy
#~ msgid "Configure Layouts..."
#~ msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ് &പ്രാവര്‍ത്തികമാക്കുക"

#~ msgid "Keyboard Repeat"
#~ msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ്"

#~ msgid "Turn o&ff"
#~ msgstr "ഓ&ഫാക്കുക"

#, fuzzy
#~ msgid "Configure..."
#~ msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ് &പ്രാവര്‍ത്തികമാക്കുക"

#~ msgid ""
#~ "If supported, this option allows you to hear audible clicks from your "
#~ "computer's speakers when you press the keys on your keyboard. This might "
#~ "be useful if your keyboard does not have mechanical keys, or if the sound "
#~ "that the keys make is very soft.<p>You can change the loudness of the key "
#~ "click feedback by dragging the slider button or by clicking the up/down "
#~ "arrows on the spin box. Setting the volume to 0% turns off the key click."
#~ msgstr ""
#~ "പിന്തുണയുണ്ടെങ്കില്‍, കീബോര്‍ഡിലെ കീകള്‍ അമര്‍ത്തിയാല്‍ കമ്പ്യൂട്ടറിന്റെ സ്പീക്കറില്‍നിന്നും കേള്‍"
#~ "ക്കാവുന്ന ക്ലിക്കുകള്‍ ശ്രവിക്കാന്‍ ഈ ഐച്ഛികം നിങ്ങളെ അനുവദിക്കും. നിങ്ങളുടെ കീബോര്‍ഡിന് "
#~ "യാന്ത്രിക കീകള്‍ ഇല്ലെങ്കിലോ, അവയുണ്ടാക്കുന്ന സ്വരം വളരെ മൃദുവാണെങ്കിലോ ഇത് "
#~ "ഉപയോഗപ്രദമായേക്കാം. സ്ലൈഡര്‍ ബട്ടണ്‍ വലിച്ചോ, സ്പിന്‍ പെട്ടിയിലെ മുകളിലേക്കും താഴേക്കുമുള്ള "
#~ "ആരോകള്‍ ക്ലിക്ക് ചെയ്തോ, കീ ക്ലിക്ക് ഫീഡ്ബാക്കിന്റെ ഒച്ച നിങ്ങള്‍ക്ക് മാറ്റാവുന്നതാണ്. ഒച്ച 0% "
#~ "ആക്കിയാല്‍ കീ ക്ലിക്ക് ഓഫാകും."

#, fuzzy
#~ msgid "&Key click volume:"
#~ msgstr "കീ ക്ലിക്ക് &ഒച്ച:"

#~ msgid "XKB extension failed to initialize"
#~ msgstr "XKB എക്സ്റ്റന്‍ഷന്‍ തുടങ്ങുന്നതില്‍ പരാജയപ്പെട്ടു."

#~ msgid ""
#~ "If you check this option, pressing and holding down a key emits the same "
#~ "character over and over again. For example, pressing and holding down the "
#~ "Tab key will have the same effect as that of pressing that key several "
#~ "times in succession: Tab characters continue to be emitted until you "
#~ "release the key."
#~ msgstr ""
#~ "നിങ്ങള്‍ ഈ ഐച്ഛികം ചെക്ക് ചെയ്താല്‍, ഒരു കീ അമര്‍ത്തിപ്പിടിച്ചാല്‍ ഒരേ അക്ഷരരൂപം വീണ്ടും വീണ്ടും "
#~ "പുറത്തുവരും. ഉദാഹരണത്തിന്, ടാബ് കീ അമര്‍ത്തിപ്പിടിച്ചാല്‍ ആ കീ ഒന്നിനു പിറകേ ഒന്നായി അമര്‍"
#~ "ത്തുന്ന ഫലമുണ്ടാകും: ടാബ് അക്ഷരരൂപങ്ങള്‍ കീ വിടുന്നതുവരെ പുറത്തുവന്നുകൊണ്ടിരിക്കും."

#~ msgid "&Enable keyboard repeat"
#~ msgstr "കീബോര്‍ഡ് റിപ്പീറ്റ് &പ്രാവര്‍ത്തികമാക്കുക"
